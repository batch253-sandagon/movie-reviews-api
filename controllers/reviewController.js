// Import necessary model
const Review = require("../models/Review");

// Function to retrieve reviews
const getReviews = async (req, res) => {
    try {
        const foundReviews = await Review.find({});

        if (foundReviews) {
            res.status(200).json(foundReviews);
        } else {
            res.status(500).json(false);
        };
    } catch(err) {
        console.log(err);
        res.status(500).json(false);
    }
};

// Function to create review
const createReview = async (req, res) => {
    try {
        const {title, rating, review} = req.body;

        if(req.body){
            let newReview = new Review({
                "title": title,
                "rating": rating,
                "review": review
            });
            const savedReview = await newReview.save();
            
            if (savedReview) {
                res.status(201).json(savedReview);
            } else {
                res.status(500).json(false);
            };
        } else {
            res.status(400).json(false)
        };
    } catch(err) {
        console.log(err);
        res.status(500).json(false);
    };
};

// Function to update reviews
const updateReview = async (req, res) => {
    try {
        if (req.body) {
            const {_id, title, rating, review} = req.body;

            const updatedReview = await Review.findByIdAndUpdate(_id, {
                "title": title,
                "rating": rating,
                "review": review
            }, {new: true});

            if (updatedReview) {
                res.status(200).json(updatedReview);
            } else {
                res.status(500).json(false);
            };
        } else {
            res.status(400).json(false);
        };
    } catch(err) {
        console.log(err);
        res.status(500).json(false);
    };
};

// Function to delete reviews
const deleteReview = async (req, res) => {
    try {
        if (req.body) {
            const {_id} = req.body;

            const foundReview = await Review.findByIdAndRemove(_id, {new:true})

            if (foundReview) {
                res.status(204).json(foundReview);
            } else {
                res.status(400).json(false);
            };
        };
    } catch (err) {
        console.log(err);
        res.status(500).json(false);
    };
};

module.exports = {getReviews, createReview, updateReview, deleteReview};