// Import Mongoose package
const mongoose = require("mongoose");

const reviewSchema = mongoose.Schema({
    "title": {
        type: String,
        required: [true, "Title name is requires"]
    },
    "rating": {
        type: Number,
        min: [0, "Rating must not be below 0"],
        max: [10, "Rating must not exceed 10"],
        default: 0
    },
    "review": {
        type: String,
        required: [true, "Title name is requires"]
    }
});

// Export the Product model using the product schema
module.exports = mongoose.model("Review", reviewSchema);