// Import required dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Import routes for different resources
const reviewRoute = require("./routes/reviewRoute.js");

// Initialize express app
const app = express();

// Set up port for server to listen on
const port = 4000;

// Connect to MongoDB Atlas database
mongoose.connect("mongodb+srv://admin:admin123@b253-sandagon.ocfyhy9.mongodb.net/SimpleMovieReviewsAPI?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

// Set up a callback to log when the connection is established
const db = mongoose.connection;
db.once("open", ()=>console.log("Now connected to MongoDB Atlas."));

// Set up middleware to parse JSON and urlencoded requests
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

// Set up routes for different resources
app.use("/reviews", reviewRoute);

// If this file is being run as the main module, start the server
if(require.main === module){
    app.listen(port, () => {
      console.log(`API is now online on ${process.env.PORT || 4000}...`);  
    });
};