// Import Express package and create an Express router
const express = require("express");
const router = express.Router();

// Import reviewController
const reviewController = require("../controllers/reviewController");

router.route('/')
  .get(reviewController.getReviews)
  .post(reviewController.createReview)
  .put(reviewController.updateReview)
  .delete(reviewController.deleteReview)

module.exports = router;